# Local https proxy

Requirements
------------

```
doker > 18
doker-compose > 1.21
```

Docker
------

Use Makefile commands. To show the list of commands from Makefile with their description:

```bash
make usage
```

To run specific command from Makefile:

```bash
make {COMMAND_NAME}
```

First run
---------

```bash
make init
make start
```
